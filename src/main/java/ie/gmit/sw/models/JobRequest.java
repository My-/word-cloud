package ie.gmit.sw.models;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class JobRequest {
    private String cmbOptions;
    private String query;
}
