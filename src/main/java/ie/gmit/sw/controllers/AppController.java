package ie.gmit.sw.controllers;

import ie.gmit.sw.ai.cloud.LogarithmicSpiralPlacer;
import ie.gmit.sw.ai.cloud.WeightedFont;
import ie.gmit.sw.ai.cloud.WordFrequency;
import ie.gmit.sw.models.JobRequest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Base64;
import java.util.Comparator;

@Slf4j
@Controller
public class AppController {
    // inject via application.properties
    @Value("${welcome.message}")
    private String message;

    @Value("${file.ignored-words}")
    private String ignoredWordsFile;

    // ref: https://spring.io/guides/gs/handling-form-submission/
    @GetMapping
    public String msin(Model model) {
        model.addAttribute("message", message);
        model.addAttribute("jobRequest", new JobRequest());
//        model.addAttribute("tasks", tasks);

        return "index";
    }

    @SneakyThrows
    @PostMapping
    public String doProcess(@ModelAttribute JobRequest jobRequest, Model model) {
        var busImg = "R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==";

        var userDir = System.getProperty("user.dir");
        log.info("Your working directory is: {}", userDir);

        File file = new File(userDir + File.separator +"res"+ File.separator + ignoredWordsFile);

        // print files content to console
//        Files.lines(Paths.get(file.getAbsolutePath()))
//                .forEach(System.out::println);

        WordFrequency[] words = new WeightedFont().getFontSizes(getWordFrequencyKeyValue());
        Arrays.sort(words, Comparator.comparing(WordFrequency::getFrequency, Comparator.reverseOrder()));
        //Arrays.stream(words).forEach(System.out::println);

        //Spira Mirabilis
        LogarithmicSpiralPlacer placer = new LogarithmicSpiralPlacer(800, 600);
        for (WordFrequency word : words) {
            placer.place(word); //Place each word on the canvas starting with the largest
        }

        BufferedImage cloud = placer.getImage();

        var cloudImg = encodeToString(cloud);

        model.addAttribute(file);
        model.addAttribute(jobRequest);
        model.addAttribute("cloudImage", cloudImg);

        return "display";
    }

    //A sample array of WordFrequency for demonstration purposes
    private WordFrequency[] getWordFrequencyKeyValue() {
        WordFrequency[] wf = new WordFrequency[32];
        wf[0] = new WordFrequency("Galway", 65476);
        wf[1] = new WordFrequency("Sligo", 43242);
        wf[2] = new WordFrequency("Roscommon", 2357);
        wf[4] = new WordFrequency("Clare", 997);
        wf[5] = new WordFrequency("Donegal", 876);
        wf[17] = new WordFrequency("Armagh", 75);
        wf[6] = new WordFrequency("Waterford", 811);
        wf[7] = new WordFrequency("Tipperary", 765);
        wf[8] = new WordFrequency("Westmeath", 643);
        wf[9] = new WordFrequency("Leitrim", 543);
        wf[10] = new WordFrequency("Mayo", 456);
        wf[11] = new WordFrequency("Offaly", 321);
        wf[12] = new WordFrequency("Kerry", 221);
        wf[13] = new WordFrequency("Meath", 101);
        wf[14] = new WordFrequency("Wicklow", 98);
        wf[18] = new WordFrequency("Antrim", 67);
        wf[3] = new WordFrequency("Limerick", 1099);
        wf[15] = new WordFrequency("Kildare", 89);
        wf[16] = new WordFrequency("Fermanagh", 81);
        wf[19] = new WordFrequency("Dublin", 12);
        wf[20] = new WordFrequency("Carlow", 342);
        wf[21] = new WordFrequency("Cavan", 234);
        wf[22] = new WordFrequency("Down", 65);
        wf[23] = new WordFrequency("Kilkenny", 45);
        wf[24] = new WordFrequency("Laois", 345);
        wf[25] = new WordFrequency("Derry", 7);
        wf[26] = new WordFrequency("Longford", 8);
        wf[27] = new WordFrequency("Louth", 34);
        wf[28] = new WordFrequency("Monaghan", 101);
        wf[29] = new WordFrequency("Tyrone", 121);
        wf[30] = new WordFrequency("Wexford", 144);
        wf[31] = new WordFrequency("Cork", 522);
        return wf;
    }

    private String encodeToString(BufferedImage image) {
        String s = null;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, "png", bos);
            byte[] bytes = bos.toByteArray();

            Base64.Encoder encoder = Base64.getEncoder();
            s = encoder.encodeToString(bytes);
            bos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return s;
    }

    private BufferedImage decodeToImage(String imageString) {
        BufferedImage image = null;
        byte[] bytes;
        try {
            Base64.Decoder decoder = Base64.getDecoder();
            bytes = decoder.decode(imageString);
            ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
            image = ImageIO.read(bis);
            bis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }
}
